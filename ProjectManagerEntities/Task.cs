﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;


namespace ProjectManagerEntitiesLib
{
    public class Task
    {
        [Key]
        public int Task_ID { get; set; }
      
       
        public int? Parent_ID { get; set; }
        [Required]    
      
        public int Project_ID { get; set; }
        [Required]
        public string TaskName { get; set; }
        [Required]
        public int Priority { get; set; }
        [Required]
        public DateTime Start_Date { get; set; }
        [Required]
        public DateTime End_Date { get; set; }
        [Required]
     
        public int TASK_OWNER_ID { get; set; }
        [Required]
        public int Status { get; set; }

        [ForeignKey("Parent_ID")]
        public ParentTask ParentTask { get; set; }

        [ForeignKey("Project_ID")]
        public Project Project { get; set; }

        [ForeignKey("TASK_OWNER_ID")]
        public User  User { get; set; }

    }
}
