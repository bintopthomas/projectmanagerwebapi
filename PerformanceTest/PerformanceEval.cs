﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NBench;
using NBench.Util;
using ProjectManagerBusinessLib;
using ProjectManagerEntitiesLib;
namespace PerformanceTest
{
    public class PerformanceEval
    {
        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
           NumberOfIterations = 1, RunMode = RunMode.Throughput,
           RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkAddUpdate()
        {

            Task task = new Task();
            task.TaskName = "test111";
            task.Parent_ID = 4;
            task.Start_Date = System.DateTime.Now;
            task.End_Date = System.DateTime.Now;
            task.Project_ID = 1;
            task.Status = 0;
            task.Priority = 12;
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.AddTask(task);
            obj.UpdateTask(task);
            obj.UpdateEndDate(task);
            //obj.GetAll();
            //obj.GetByTaskName("test111");
            //obj.GetById(8051);
        }
        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
         NumberOfIterations = 1, RunMode = RunMode.Throughput,
         RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkAddUpdateUser()
        {

            User user = new User();
            user.FirstName = "PerFirstName";
            user.LastName = "PerLastName";
            user.EmployeeId = 5436;
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.AddUser(user);
            obj.UpdateUser(user);
            
            //obj.GetAll();
            //obj.GetByTaskName("test111");
            //obj.GetById(8051);
        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
        NumberOfIterations = 1, RunMode = RunMode.Throughput,
        RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkAddUpdateProject()
        {

            Project project = new Project();
            project.Project_Name = "PerProjectName";
            project.Priority = 12;
            project.Start_Date = System.DateTime.Now;
            project.End_Date = System.DateTime.Now;
            project.Manager_ID = 1;
           
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.AddProject(project);
            obj.UpdateProject(project);

            
        }
        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
       NumberOfIterations = 1, RunMode = RunMode.Throughput,
       RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [CounterThroughputAssertion("TestCounter", MustBe.LessThan, 10000000.0d)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkAddParentTask()
        {

            ParentTask ptask = new ParentTask();
            ptask.Parent_Task = "PerTask";
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.AddParentTask(ptask);
            


        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
            NumberOfIterations = 1, RunMode = RunMode.Throughput,
            RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkView()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.GetAllTask();
            obj.GetAllUser();
            obj.GetAllParentTask();
            obj.GetAllProject();
            //obj.GetByTaskName("test111");
            //obj.GetById(8051);
        }

        [PerfBenchmark(Description = "Test to ensure that a minimal throughput test can be rapidly executed.",
          NumberOfIterations = 1, RunMode = RunMode.Throughput,
          RunTimeMilliseconds = 15, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThanOrEqualTo, 70000000)]
        [GcTotalAssertion(GcMetric.TotalCollections, GcGeneration.Gen2, MustBe.LessThanOrEqualTo, 4.0d)]
        public void BenchmarkGetByTaskId()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.GetByTaskId(7);
        }

        [PerfCleanup]
        public void Cleanup()
        {
            // does nothing
        }
    }
}
