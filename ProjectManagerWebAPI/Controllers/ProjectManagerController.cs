﻿using ProjectManagerBusinessLib;
using ProjectManagerEntitiesLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectManagerWebAPI.Controllers
{
    public class ProjectManagerController : ApiController
    {

        
        [Route("PostUser")]
        [HttpPost]
        public List<User> PostUser(User user)
        {
            
            ProjectManagerBusiness obj = new ProjectManagerBusiness();           

           return obj.AddUser(user);
           

        }
        [Route("DeleteUser")]
        [HttpDelete]
        public List<User> DeleteUser(int id)
        {
            //var response = Request.CreateResponse(HttpStatusCode.OK);
            ProjectManagerBusiness obj = new ProjectManagerBusiness();

            return obj.DeleteUser(id);


        }
        [Route("UpdateUser")]
        [HttpPut]
        public IHttpActionResult PutUser(User user)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.UpdateUser(user);
            return Ok("Record Updated");
        }

        [Route("GetAllUser")]
        [HttpGet]
        public List<User> GetAllUser()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetAllUser();

        }

        [Route("PostProject")]
        [HttpPost]
        public int PostProject(Project project)
        {
            if (project.Start_Date.ToString() == "01-01-0001 00:00:00")
                project.Start_Date = null;
            if (project.End_Date.ToString() == "01-01-0001 00:00:00")
                project.End_Date = null;
            
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.AddProject(project);
            

        }
        [Route("DeleteProject")]
        [HttpDelete]
        public List<Project> DeleteProject(int id)
        {
            //var response = Request.CreateResponse(HttpStatusCode.OK);
            ProjectManagerBusiness obj = new ProjectManagerBusiness();

            return obj.DeleteProject(id);


        }
        [Route("UpdateProject")]
        [HttpPut]
        public IHttpActionResult PutProject(Project project)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.UpdateProject(project);
            return Ok("Record Updated");
        }

        [Route("GetAllProject")]
        [HttpGet]
        public List<Project> GetAllProject()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetAllProject();

        }

        [Route("GetAllParentTask")]
        [HttpGet]
        public List<ParentTask> GetAllParentTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetAllParentTask();

        }


        [Route("PostTask")]
        [HttpPost]
        public int PostTask(Task task)
        {
            
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.AddTask(task);           

        }
        [Route("DeleteTask")]
        [HttpDelete]
        public List<Task> DeleteTask(int id)
        {
            //var response = Request.CreateResponse(HttpStatusCode.OK);
            ProjectManagerBusiness obj = new ProjectManagerBusiness();

            return obj.DeleteTask(id);


        }
        [Route("UpdateTask")]
        [HttpPut]
        public IHttpActionResult PutTask(Task task)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.UpdateTask(task);
            return Ok("Record Updated");
        }
        [Route("EndTask")]
        [HttpPut]
        public List<Task> UpdateEndDate(Task item)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.UpdateEndDate(item);
        }
        [Route("GetAllTask")]
        [HttpGet]
        public List<Task> GetAllTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetAllTask();

        }
        [Route("GetByTaskName")]
        [HttpGet]
        public Task GetByTaskName(string TaskName)
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetByTaskName(TaskName);

        }
        [Route("GetByTaskId")]
        [HttpGet]
        public Task GetByTaskId(int TaskId)
        {

            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            return obj.GetByTaskId(TaskId);

        }

        [Route("PostParentTask")]
        [HttpPost]
        public IHttpActionResult PostParentTask(ParentTask parenttask)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            obj.AddParentTask(parenttask);
            return Ok("Record Added");

        }
    }
}
