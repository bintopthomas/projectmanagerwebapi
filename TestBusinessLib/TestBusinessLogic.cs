﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ProjectManagerBusinessLib;
using ProjectManagerEntitiesLib;
namespace TestBusinessLib
{
    [TestFixture]
    public class TestBusinessLogic
    {
        [Test]
        public void TestUserGetAll()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            int actual = obj.GetAllUser().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestAddUser()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            User item = new User();
            item.FirstName = "Kanthi";
            item.LastName = "Sharma";
            item.EmployeeId = 5643;            
            obj.AddUser(item);
            int actual = obj.GetAllUser().Count;
            Assert.Greater(actual, 4);

        }
        [Test]
        public void TestParentTaskGetAll()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            int actual = obj.GetAllParentTask().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestAddParentTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            ParentTask item = new ParentTask();
            item.Parent_Task = "IIS";           
            obj.AddParentTask(item);
            int actual = obj.GetAllParentTask().Count;
            Assert.Greater(actual, 2);

        }
        [Test]
        public void TestDeleteUser()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            List<User> user =  obj.DeleteUser(1002);          
            Assert.Greater(user.Count(), 1);

        }
        [Test]
        public void TestDeleteProject()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            List<Project> user = obj.DeleteProject(4);
            Assert.Greater(user.Count(), 1);

        }
        [Test]
        public void TestDeleteTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            List<Task> task = obj.DeleteTask(1004);
            Assert.Greater(task.Count(), 1);

        }
        [Test]
        public void TestProjectGetAll()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            int actual = obj.GetAllProject().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestAddProject()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Project item = new Project();
            item.Project_Name = "ProjectTestVL";
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now.AddDays(1);
            item.Priority = 12;
            item.Manager_ID = 1;
            obj.AddProject(item);
            int actual = obj.GetAllProject().Count;
            Assert.Greater(actual, 1);

        }
        [Test]
        public void TestUpdateProject()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Project item = new Project();
            item.Project_Name = "ProjectTestVL";
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now.AddDays(1);
            item.Priority = 12;
            item.Manager_ID = 1;
            obj.AddProject(item);
            int actual = obj.GetAllProject().Count;
            Assert.Greater(actual, 3);

        }
        [Test]
        public void TestUpdateUser()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            User item = new User();
            item.User_ID = 1;       
            item.FirstName = "Roshan";
            item.LastName = "Jagadish";
            item.EmployeeId = 1234;
            obj.UpdateUser(item);
            int actual = obj.GetAllUser().Count;
            Assert.Greater(actual, 1);

        }

        [Test]
        public void TestGetAll()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            int actual = obj.GetAllTask().Count;
            Assert.Greater(actual, 0);
        }
        [Test]
        public void TestGetByTaskId()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Task item = obj.GetByTaskId(1);
            Assert.AreEqual(1, item.Task_ID);
        }
        [Test]
        public void TestAddTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Task item = new Task();
            item.TaskName = "DeploymentAT";            
            item.Priority = 15;
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now;
            item.Status = 0;
            item.Project_ID = 1;
            item.TASK_OWNER_ID = 1;
            obj.AddTask(item);
            Task test = obj.GetByTaskName("DeploymentAT");
            Assert.AreEqual("DeploymentAT", test.TaskName);

        }
        //[Test]
        //public void TestDeleteTask()
        //{

        //    ProjectManagerBusiness obj = new ProjectManagerBusiness();
        //    obj.DeleteTask(6024);
        //    Task item = obj.GetByTaskId(6024);
        //    Assert.AreEqual(null, item);
        //}
        [Test]
        public void TestUpdateTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Task item = new Task();
            item.Task_ID = 1;
            item.TaskName = "DeploymentAT";
            item.Priority = 18;
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now;
            item.Project_ID = 1;
            item.TASK_OWNER_ID = 1;
            item.Status = 0;
            obj.UpdateTask(item);
            Task itemafterupdate = obj.GetByTaskName("DeploymentAT");
            Assert.AreEqual(18, itemafterupdate.Priority);

        }
        [Test]
        public void TestUpdateEndTask()
        {
            ProjectManagerBusiness obj = new ProjectManagerBusiness();
            Task item = new Task();
            item.Task_ID = 1;
            item.TaskName = "DeploymentAT";
            item.Priority = 1;
            item.Start_Date = System.DateTime.Now;
            item.End_Date = System.DateTime.Now;
            item.Status = 0;
            obj.UpdateEndDate(item);
            Task itemafterupdate = obj.GetByTaskName("DeploymentAT");
            Assert.AreEqual(1, itemafterupdate.Status);


        }
    

}
}
